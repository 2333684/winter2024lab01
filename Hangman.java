import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char letter) {
	for (int i =0; i<4 ; i++){
	
		if (word.charAt(i) == letter){
			return i;
		}
	}
	   return -1;
	}
	public static char toUpperCase(char letter) {
		
		letter = Character.toUpperCase(letter);
		return letter;
	}
	public static void printWork(String word, boolean letterStatus []) {
		String newWord = "";
		
		for (int i = 0; i < 4 ;i++) {
			if( letterStatus[i] == true) {
				newWord += word.charAt(i);
			}else {
			newWord += " _ ";
			}
		}
		System.out.println("Your result is "+ newWord);
	}
	public static  void runGame(String word){
		
		boolean letterStatus [] = new boolean []{false,false,false,false};
		int wrongGuessCount = 0;
	
		while(wrongGuessCount < 6 && !(letterStatus[0] && letterStatus[1] && letterStatus[2] && letterStatus[3])){
	
			Scanner reader = new Scanner (System.in);
			
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess =toUpperCase(guess);
			
			
		/*Checks if isLetterInWord = to an index of letterStatus[]
			if yes the index of letterStatus = true */
			int index = isLetterInWord(word, guess);
				if(index >= 0 && index <= 3) {
					letterStatus[index] = true;
				}else {
					wrongGuessCount++;
				}
			
			printWork(word, letterStatus);
		}
		if (wrongGuessCount == 6){
			System.out.println("Oops! Better luck next time :)" );
		}
		else{
			System.out.println("Congrats! You got it :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}